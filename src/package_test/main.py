
from getrak.apis.localizacoes import Localizacoes 
from getrak.apis.recebidos import Recebidos
from getrak.api import Api
import bottle
from bottle import get, error

app = bottle.default_app()

@get('/api/test')
def test_api():
  return 'ok'

if __name__ == '__main__':
  bottle.run(app, host='localhost', port=8000)

